const puppeteer = require('puppeteer')
const selector = require('./selector/interwin.js')
var fs = require('fs')
const options = {
    width: 1366,
    height: 768
}
const proxyConfig = {
    host: 'http://108.61.250.163:5555',
    username: 'ibn',
    password: 'temp8888'
}
const detail = {
    url: 'https://www.betstreet.net',
    agentCode: 'vegasbet',
    email: 'evnvx@gmail.com',
    password: 'zxcv1234'
}

const start = async () => {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--windows-size=${options.width},${options.height}`,
            `--proxy-server=${proxyConfig.host}`
        ]
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: options.width,
        height: options.height
    })
    await page.authenticate({
        username: proxyConfig.username,
        password: proxyConfig.password
    })

    await page.goto(detail.url, {
        waitUntil: 'load'
    })
    await page.waitForSelector(selector.agentCode, {
        timeout: 10000
    })

    await page.type(selector.agentCode, detail.agentCode)
    await page.type(selector.email, detail.email)
    await page.type(selector.password, detail.password)
    await page.click(selector.signinButton)

    await page.waitForSelector(selector.dashboard, {
        timeout: 20000
    })
    await page.click(selector.transaction)
    

    await Promise.all([
        page.click(selector.recordTrans),
        page.waitForNavigation({ waitUntil: 'load' })
    ]);

    await page.waitForFunction(selector => document.querySelector(selector.depoDateSelect).value, {}, selector);
    await page.click(selector.depoDateSelect)
    //console.log('2')
    await page.waitForSelector(selector.depoDateMonth, {
        timeout: 10000,
        visible:true
    })
    await page.click(selector.depoDateMonth)
    await page.waitForSelector(selector.depoDateMonth, {
        timeout: 10000,
        visible:false
    })
    await page.click(selector.depoStatus)
    await page.waitForSelector(selector.statusDropDown, {
        timeout: 10000,
        visible:true
    })
    await page.click(selector.depoConfirm)
    // await page.waitForFunction(selector => document.querySelector(selector.depoConfirm).value, {}, selector);
    await page.click(selector.depoDebit)
    await page.click(selector.searchButton)
    await page.waitForSelector(selector.depoCat, {
      hidden:true,
    })
    await page.waitForSelector(selector.depoEntries,{
        timeout:10000
    })
     await page.select(selector.depoEntries, '-1')
     await page.waitForFunction(selector => document.querySelector(selector.depoEntries).value.includes("-1"), {}, selector);
        
        const dataTransaction = await page.evaluate(selector => {
            const pageAllTransaction = []
            let table = document.querySelectorAll('#DataTables_Table_0 > tbody > tr')
            for (let o = 0; o < table.length; o++) {
                let tempRow = table[0]
                let transaction = tempRow.querySelectorAll('tr>td')
                let transactionData = {}
                transactionData.id = transaction[0].innerText
                transactionData.transactionDate = transaction[1].innerText
                transactionData.transactionID = transaction[2].innerText
                transactionData.accountName = transaction[3].innerText
                transactionData.accountCode = transaction[4].innerText
                transactionData.bankName = transaction[5].innerText
                transactionData.status = transaction[6].innerText
                transactionData.receipt = transaction[7].innerText
                transactionData.refNo = transaction[8].innerText
                transactionData.debit = transaction[9].innerText
                transactionData.credit = transaction[10].innerText
                transactionData.confirmed = transaction[11].innerText
                transactionData.confirmedTime = transaction[12].innerText
                transactionData.reason = transaction[13].innerText
                
                pageAllTransaction.push(transactionData)
                console.log(transactionData)
            }
            return pageAllTransaction
            console.log(pageAllTransaction)
        }, selector)
        //console.log('1')
        fs.writeFile('interwinDepo.json', JSON.stringify(dataTransaction), function (err) {
            if (err) throw err;
            console.log('Saved!');
          });
}

start()
