const puppeteer = require('puppeteer')
const selector = require('./selector/idn.js')
var fs = require('fs');
const dateNow = '19-11-2017'
const options = {
    width: 1366,
    height: 768
}
const proxyConfig = {
    host: 'http://108.61.250.163:5555',
    username: 'ibn',
    password: 'temp8888'
}
const detail = {
    url: 'https://ag.suksesbogil.com/index.php',
    username: 'wjlaainv',
    password: 'asdqwe78##'
}

const start = async (dateNow) => {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--windows-size=${options.width},${options.height}`,
            `--proxy-server=${proxyConfig.host}`
        ]
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: options.width,
        height: options.height
    })
    await page.authenticate({
        username: proxyConfig.username,
        password: proxyConfig.password
    })

    await page.goto(detail.url, {
        waitUntil: 'load'
    })
    await page.waitForSelector(selector.username, {
        timeout: 10000
    })

    await page.type(selector.username, detail.username)
    await page.type(selector.password, detail.password)
    await page.click(selector.signinButton)

    await page.waitForSelector(selector.submitButton, {
        timeout: 10000
    })
    await page.evaluate(selector => {
        document.querySelector(selector.pin).value = "1234"
    }, selector)
    await Promise.all([
        page.click(selector.submitButton),
        page.waitForNavigation({ waitUntil: 'load' })
    ]);
    const masterFrame = await page.frames().find(frame => frame.name() === 'master')
    const menuFrame = await page.frames().find(frame => frame.name() === 'menu')
    if (menuFrame) {
        await Promise.all([
            menuFrame.click(selector.completeTransaction),
            masterFrame.waitForNavigation({waitUntil :'load'})
        ]) 
    }

    if (masterFrame.url() === "https://ag.suksesbogil.com/agentclosingbet.php") {
        await masterFrame.evaluate((selector, dateNow) => {
            dateNow = dateNow.concat(' 00:00:00')
            document.querySelector(selector.dateStart).value = dateNow
        }, selector, dateNow)
    }
 
        await Promise.all([
            masterFrame.click(selector.submitTransaction),
            masterFrame.waitForNavigation({waitUntil :'load'})
        ]) 
    const dataTransaction = await masterFrame.evaluate(selector => {
        let totalData = {}
        let newLine = document.querySelector(selector.pageNumber).innerText.split(/\n/)[0].split(" ")
        totalData.dataShow = newLine[0]
        totalData.dataAll = newLine[2]
        return totalData
    }, selector)
    console.log(dataTransaction.dataShow)
    let pageNumber = dataTransaction ? (dataTransaction.dataAll / dataTransaction.dataShow) + 1 : 0
    console.log(pageNumber)
    let allMember = []

    for (let i = 0; i < pageNumber; i++) {
        console.log(i)
    
    // await masterFrame.waitForSelector(selector.firstButton)
    const baseData = await masterFrame.evaluate(selector => {

        let table = document.querySelectorAll(selector.tableTransaction)
        let allData = []
        

        for(let x=2;x<table.length-3;x++){
        let transactionData = {}
        let tempRow = table[x]
        let transaction = tempRow.querySelectorAll('tr>td')
        
        transactionData.id = transaction[0].innerText
        transactionData.userID = transaction[1].innerText
        transactionData.turnOver = transaction[2].innerText
        transactionData.winLose = transaction[3].innerText
        transactionData.agentCommission = transaction[4].innerText
        transactionData.agentInquiry = transaction[5].innerText
        transactionData.url = transaction[1].querySelector('a').href
        allData.push(transactionData)
        }
        return allData
    }, selector)

    

    for(let y=0;y<baseData.length;y++){

        const pageDetail = await browser.newPage()
        await pageDetail.goto(baseData[y].url, {
        waitUntil: 'load'
        })

        const tableDetail = await pageDetail.evaluate(selector => {
        let detailAll = []
        let table = document.querySelectorAll(selector.tableDetail)
        

        for (let o = 2; o < table.length -2; o++) {
            let detailTransaction = {}
            let tempRow = table[o]
            let transaction = tempRow.querySelectorAll('tr>td')
            detailTransaction.Game = transaction[1].innerText
            detailTransaction.TurnOver = transaction[2].innerText
            detailTransaction.WinLose = transaction[3].innerText
            detailTransaction.AgentCommission = transaction[4].innerText
            detailTransaction.AgentInquiry = transaction[5].innerText
            detailAll.push(detailTransaction)
        }
        return detailAll
    }, selector)
   // console.log(tableDetail)
    baseData[y].detail = tableDetail
    allMember.push(baseData[y])
    await pageDetail.waitForSelector(selector.tableDetail,{timeout:10000})
    await pageDetail.close()
    }
    masterFrame.click(selector.nextButton)
    }

    fs.writeFile('outputIDN.json', JSON.stringify(allMember), function (err) {
        if (err) throw err;
        console.log('Saved!');
      });


}

start(dateNow)