const puppeteer = require('puppeteer')
const selector = require('./selector/idn.js')
var fs = require('fs');
const dateNow = '19-11-2017'
const options = {
    width: 1366,
    height: 768
}
const proxyConfig = {
    host: 'http://108.61.250.163:5555',
    username: 'ibn',
    password: 'temp8888'
}
const detail = {
    url: 'https://ag.suksesbogil.com/index.php',
    pasaranUrl: 'https://ag.suksesbogil.com/admin_invoice13.php?psr=',
    username: 'wjlaainv',
    password: 'asdqwe78##'
}

const start = async (dateNow) => {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--windows-size=${options.width},${options.height}`,
            `--proxy-server=${proxyConfig.host}`
        ]
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: options.width,
        height: options.height
    })
    await page.authenticate({
        username: proxyConfig.username,
        password: proxyConfig.password
    })

    await page.goto(detail.url, {
        waitUntil: 'load'
    })
    await page.waitForSelector(selector.username, {
        timeout: 10000
    })

    await page.type(selector.username, detail.username)
    await page.type(selector.password, detail.password)
    await page.click(selector.signinButton)

    await page.waitForSelector(selector.submitButton, {
        timeout: 10000
    })
    await page.evaluate(selector => {
        document.querySelector(selector.pin).value = "1234"
    }, selector)

    await Promise.all([
        page.click(selector.submitButton),
        page.waitForNavigation({ waitUntil: 'load' })
    ]);
    const masterFrame = await page.frames().find(frame => frame.name() === 'master')
    const menuFrame = await page.frames().find(frame => frame.name() === 'menu')
    if (menuFrame) {
        await Promise.all([
            menuFrame.click(selector.singaporeTransaction),
            masterFrame.waitForNavigation({waitUntil :'load'})
        ]) 
    }
    if (masterFrame.url() === `${detail.pasaranUrl}p759`) {
        await masterFrame.evaluate((selector) => {
            document.querySelector(selector.periode).value = '622'
        }, selector)
    }
    await Promise.all([
        masterFrame.click(selector.submitPasaran),
        masterFrame.waitForNavigation({waitUntil :'load'})
    ]) 
    const combineLine = await masterFrame.evaluate((selector) => {
        let firstLine = document.querySelector(selector.firstLine).innerText.replace(/\s/g, "").split(":")
        let secondLine = document.querySelector(selector.secondLine).innerText.replace(/\s/g, "").split(":")

        firstLine.shift()
        secondLine.shift()

        firstLine = firstLine.concat(secondLine)

        return firstLine
        
    }, selector)

    const firstLineResult = await masterFrame.$eval(selector.firstLine,el=>el.innerText.replace(/\s/g,"").split(":").splice(1, 8))
    const secondLineResult = await masterFrame.$eval(selector.secondLine,el=>el.innerText.replace(/\s/g,"").split(":").splice(1, 8))

    console.log(firstLineResult)
    console.log(secondLineResult)
    for(let z=0;z<firstLineResult.length;z++){
        if(firstLineResult[z] !== "0"){
           await masterFrame.evaluate((selector,z)=>{
                
                document.querySelectorAll(selector.firstLineButton)[z].click()                
                return true
            },selector,z)
            

            await masterFrame.waitFor(2500)
                
                const childFrame = await page.frames().find(frame => frame.name() === 'tempat')

                await childFrame.waitForSelector(selector.pasaranTable)

               const table = await childFrame.evaluate(selector => {
                let table=document.querySelectorAll(selector.pasaranTable)
                let tableData = []
                for(let i=2;i<table.length-1;i++){
                    let detailTransaction = {}
                    let tempRow = table[i]
                    let transaction = tempRow.querySelectorAll('tr>td')
                    console.log(transaction)
                    detailTransaction.no = transaction[0].innerText
                    detailTransaction.periode = transaction[1].innerText
                    detailTransaction.invoice = transaction[2].innerText
                    detailTransaction.date = transaction[3].innerText
                    detailTransaction.user = transaction[4].innerText
                    detailTransaction.tebak = transaction[5].innerText
                    detailTransaction.bet = transaction[6].innerText
                    detailTransaction.disc = transaction[7].innerText
                    detailTransaction.status = transaction[8].innerText
                    detailTransaction.payment = transaction[9].innerText
                    detailTransaction.multiply = transaction[10].innerText
                    detailTransaction.reff = transaction[11].innerText
                    tableData.push(detailTransaction)
                }
                console.log(tableData)
                return tableData
               },selector)
            
               console.log(table)
            // const children = await masterFrame.evaluate((selector)=>{
               
            //     let table = document.firstChild.querySelectorAll(pasaranTable)            
            //     return table
            // },selector)
          
            // console.log(children)
        
        }
    }

    // for(let z=0;z<secondLineResult.length;z++){
    //     if(secondLineResult[z] !== "0"){
    //        const click = await masterFrame.evaluate((selector,z)=>{
               
    //             document.querySelectorAll(selector.secondLineButton)[z].click()                
    //             return true
    //         },selector,z)

    //         await masterFrame.waitFor(2500)

    //         console.log("success2")
    //     }
    // }

   

    // fs.writeFile('output.txt', JSON.stringify(allMember), function (err) {
    //     if (err) throw err;
    //     console.log('Saved!');
    //   });


}

start()