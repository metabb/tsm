const selector = {
    username:'#input-reset-1',
    password:'#Password',
    loginButton:'#btn_login',
    dashboard:'#pageTitle > div > span',
    finance:'#mCSB_1_container > li > ul > li:nth-child(4)',
    depoInfo:'#mCSB_1_container > li > ul > li.menu-item.has-sub.active > div > ul > li.DtInfoaspxOrder0 > a',
    wdInfo : '#mCSB_1_container > li > ul > li.menu-item.has-sub.active > div > ul > li.WdInfoaspxOrder1 > a',
    depoStartDate : '#selectdate_start',
    depoStatus: '#frmShowList > div.boxgy > table > tbody > tr:nth-child(2) > td:nth-child(2) > select',
    depoSearchButton : '#frmShowList > div.boxgy > div > div > ul > li:nth-child(1) > a',
    currentPage : '#current_page',
    maxPage : '#max_page',
    depoTable : '#DtInfoList > table > tbody > tr',
    nextButton : '#DtInfoList > div > div > a.next',
    wdTable : '#WdInfoList > table > tbody>tr',
    wdNextButton : '#WdInfoList > div > div > a.next',

 }
 
 module.exports = selector