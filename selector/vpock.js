const selector = {
    username:'#txtUserName',
    password:'#password',
    login:'#btnSignIn',
    capchaImg: '#form1 > table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > img',
    capchaInput: '#txtCode',
    passcode: '#txtCode',
    passcodeSubmitButton : '#divSubmit',
    okButton : '#ext_warning > div:nth-child(2) > input[type=button]',
    memberMenu : '#panMemberManagement',
    memberMenuDown : '#MemberManagement > span',
    member : '#MemberManagementMenu > div:nth-child(1) > a',
    searchButton : '#MemberList_cm1_btnSearch',
    memberTable : '#MemberList_cm1_g > tbody > tr',
    menuTable : 'table > tbody > tr > td > a'

    

 }
 
 module.exports = selector