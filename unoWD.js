const puppeteer = require('puppeteer')
const selector = require('./selector/uno.js')
var fs = require('fs');
const dateNow = '11/10/2017'
const options = {
    width: 1366,
    height: 768
}
const proxyConfig = {
    host: 'http://108.61.250.163:5555',
    username: 'ibn',
    password: 'temp8888'
}
const detail = {
    url: 'http://admin.unobet.com',
    username: 'bot',
    password: 'Abcd1212*'
}

const start = async (dateNow) => {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--windows-size=${options.width},${options.height}`,
            `--proxy-server=${proxyConfig.host}`
        ]
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: options.width,
        height: options.height
    })
    await page.authenticate({
        username: proxyConfig.username,
        password: proxyConfig.password
    })

    // page.on('framenavigated',frame =>{
    //     console.log(frame.childFrames())
    //     console.log(frame.url() + " Navigated")
    // })

    // page.on('frameattached',frame =>{
    //     console.log(frame.childFrames())
    //     console.log(frame.url() + "  Attached")
    // })

    // page.on('framedetached',frame =>{
    //     console.log(frame.childFrames())
    //     console.log(frame.url() + " Detached")
    // })

    await page.goto(detail.url, {
        waitUntil: 'load'
    })
    await page.waitForSelector(selector.username, {
        timeout: 10000
    })

    await page.type(selector.username, detail.username)
    await page.type(selector.password, detail.password)
    await page.click(selector.loginButton)
    await page.waitForSelector(selector.finance,{timeout:10000})
    await page.click(selector.finance,{timeout:10000})

    const link = await page.evaluate(selector=>{
        let link = document.querySelector(selector.wdInfo).href
        return link
    },selector)

    const page2 = await browser.newPage()
    await page2.goto(link, {
        waitUntil: 'load'
    })

    await page2.waitForSelector(selector.depoStartDate)

    await page2.evaluate((selector,dateNow)=>{
        document.querySelector(selector.depoStartDate).value = dateNow
    },selector,dateNow)
    
    await page2.evaluate(selector=>{
        document.querySelector(selector.depoStatus).value=""
    },selector)

    await page2.click(selector.depoSearchButton)
   
    await page2.waitForSelector(selector.maxPage)
    const maxPage = await page2.$eval(selector.maxPage,el=>el.value)

    console.log(maxPage)
    let totalTransaction = []

    for(let x=0;x<maxPage;x++){

    const dataTransaction = await page2.evaluate(selector=>{
        let table = document.querySelectorAll(selector.wdTable)
        console.log(table)
        let allDataTransaction = []

        for(let i=1;i<table.length;i++){
            let row = table[i]
            let transaction = row.querySelectorAll('tr>td')
            let dataTransaction = {}

            dataTransaction.trxID = transaction[0].innerText
            dataTransaction.date = transaction[1].innerText
            dataTransaction.username = transaction[2].innerText
            dataTransaction.ccy = transaction[3].innerText
            dataTransaction.amount = transaction[4].innerText
            dataTransaction.mode = transaction[5].innerText
            dataTransaction.bonusCode = transaction[6].innerText
            dataTransaction.status = transaction[7].innerText
            dataTransaction.noW = transaction[8].innerText
            dataTransaction.toAccNo = transaction[9].innerText
            dataTransaction.toAccName = transaction[10].innerText
            dataTransaction.bankType = transaction[11].innerText
            dataTransaction.remak = transaction[12].innerText
          



            allDataTransaction.push(dataTransaction)
        }
    
        console.log(allDataTransaction)
        return allDataTransaction
    },selector)
    console.log(x)
    totalTransaction = totalTransaction.concat(dataTransaction)
    await page2.click(selector.wdNextButton)
    await page2.waitForSelector(selector.maxPage)

}

await page2.close()

    fs.writeFile('unoWD.json', JSON.stringify(totalTransaction), function (err) {
        if (err) throw err;
        console.log('Saved!');
      });
}

start(dateNow)