const puppeteer = require('puppeteer');
const selector = require('./selector/vpock.js');
const Tesseract = require('tesseract.js')
const filename = 'capca.png'

const config = {
    lang: "eng",
    oem: 1,
    psm: 3,
  }

var fs = require('fs');
const dateNow = '11/10/2017'
const options = {
    width: 1366,
    height: 768
}
const proxyConfig = {
    host: 'http://108.61.250.163:5555',
    username: 'ibn',
    password: 'temp8888'
}
const detail = {
    url: 'http://mkodep2anb.onecapsa.com',
    username: 'b6ubot',
    password: 'asdjkl9900',
}

const start = async (dateNow) => {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--windows-size=${options.width},${options.height}`,
            `--proxy-server=${proxyConfig.host}`
        ]
    })
    const page = await browser.newPage()
    await page.setViewport({
        width: options.width,
        height: options.height
    })
    await page.authenticate({
        username: proxyConfig.username,
        password: proxyConfig.password
    })
    await page.goto(detail.url, {
        waitUntil: 'load'
    })
    await page.waitForSelector(selector.capchaImg, {
        timeout: 10000
    })

    const capcha = await page.$(selector.capchaImg)
    await capcha.screenshot({
        path :'capca.png'
    })

//    const cimg =  await fs.readFile('capca.png', (err, data) => {
//         if (err) throw err;
//         console.log(data);
//       });
    
    // await page.type(selector.username, detail.username)

    // Tesseract.recognize(cimg,{lang:"eng"}).then((result)=>{
    //     console.log(result)
    // })

   const capca = await Tesseract.recognize(filename)
  .progress(function  (p) {  })
    await page.type(selector.username, detail.username)
    await page.evaluate((selector,detail)=>{
        document.querySelector(selector.password).value = detail.password
        console.log(detail.password)
    },selector,detail)
    console.log(capca.text)
    await page.type(selector.capchaInput,capca.text.replace(/\s/g,''))
    await page.click(selector.login)

    await page.waitForSelector(selector.passcodeSubmitButton)

   const arrNumn = await page.evaluate(selector =>{

    const numb = []
   
    for(let j=0;j<10;j++){
        const combineNumber = `#img${j}`
        console.log(combineNumber)
        numb.push(document.querySelector(combineNumber).src.split("n=")[1])
    }

   return numb
    },selector)

    const pinVal = await page.evaluate((selector,arrNumn)=>{
        let value = ""
        for(let l=1;l<4;l++){
            for(let k=0;k<arrNumn.length;k++){
                if(arrNumn[k] == l){
                    value = value+k.toString()
                }
            }
        }
        
        value= value+value
        console.log(value)
        document.querySelector(selector.passcode).value=value
       return true
    },selector,arrNumn)

    if(pinVal) {
        await Promise.all([
            page.click(selector.passcodeSubmitButton),
            page.waitForNavigation({ waitUntil: 'load' }),
      ]);
    }

    const mainFrame = await page.frames().find(frame => frame.name() === 'fraMain')
    if(mainFrame) await mainFrame.click(selector.okButton)
    await mainFrame.waitForSelector(selector.okButton,{visible:false})
    const menuFrame = await page.frames().find(frame => frame.name() === 'fraMenu')
    if(menuFrame){
    await menuFrame.click(selector.memberMenu)
    await menuFrame.waitForSelector(selector.memberMenuDown)
    console.log("down")
    await menuFrame.waitForSelector(selector.member)
    await Promise.all([
        menuFrame.click(selector.member),
        
        mainFrame.waitForNavigation({ waitUntil: 'load' }),
    ]);
    }
    console.log("klik")
    const mainFrameMember = await page.frames().find(frame => frame.name() === 'fraMain')
    await Promise.all([
        mainFrameMember.click(selector.searchButton),
        mainFrameMember.waitForNavigation({ waitUntil: 'load' })
    ]);
    
    const table = await mainFrameMember.evaluate(selector => {
        let table=document.querySelectorAll(selector.memberTable)
        console.log(table)
        let tableData = []
        for(let i=1;i<table.length-1;i++){
            let detailTransaction = {}
            let tempRow = table[i]
            let transaction = tempRow.querySelectorAll('tr>td')
          //  console.log(transaction[1])
           detailTransaction.id = transaction[0].innerText
           detailTransaction.username = transaction[2].innerText
           detailTransaction.nickname = transaction[4].innerText
           detailTransaction.contact = transaction[5].innerText
           detailTransaction.currency = transaction[6].innerText
           detailTransaction.balance = transaction[7].innerText
           detailTransaction.loginIP = transaction[8].innerText
           detailTransaction.dateCreated = transaction[9].innerText
           detailTransaction.menu1 = transaction[1].querySelectorAll(selector.menuTable)[0].href
           detailTransaction.menu2 = transaction[1].querySelectorAll(selector.menuTable)[1].href
           detailTransaction.menu3 = transaction[1].querySelectorAll(selector.menuTable)[2].href
           detailTransaction.menu4 = transaction[1].querySelectorAll(selector.menuTable)[3].href
           detailTransaction.menu5 = transaction[1].querySelectorAll(selector.menuTable)[4].href
           tableData.push(detailTransaction)
        
        
        }
      //  console.log(tableData)
        return tableData
       },selector)

       console.log(table)


   
  //  console.log(value)

    // for(const i=0;i<9;i++){
    //     if(`${number}${i}` == 1){
    //         console.log(`${number}${i}`)
    //     }
    // }


    
}

start(dateNow)